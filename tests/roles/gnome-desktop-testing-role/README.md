gnome-desktop-testing-role
==========================

This ansible role is to make it easy to leverage gnome installed tests (via the gnome-desktop-testing module)

Requirements
------------
Xvfb

If gnome-desktop-testing isn't installed it will be built from version control.

Role Variables
--------------
The variable installed_test_name is used to describe the name of the installed tests to run.
(the basename of the directory in /usr/share/installed-tests)

Dependencies
------------
standard-test-roles

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      roles:
        - role: gnome-desktop-testing-role
          test_name: mutter
          tags:
          - classic

License
-------
BSD
